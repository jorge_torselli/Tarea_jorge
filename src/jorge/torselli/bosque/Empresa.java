package jorge.torselli.bosque;



import java.util.ArrayList;
import java.util.List;



/**
 * Created by repre on 06/07/2017.
 */
public class Empresa
{
    private String empresa;
    private List<Clientes> listadoClientes;
    private List<Compras> listadoCompras;

    public Empresa() {
    }

    public Empresa(String empresa) {
        this.empresa = empresa;
        listadoClientes = new ArrayList<>();
        listadoCompras = new ArrayList<>();
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    //*******************************************************************************************************
    //Metodos de la clase Clientes
    //*******************************************************************************************************

    //Para insertar nuevos clientes
    public void insertarCliente(Clientes cli)
    {
        listadoClientes.add(cli);
    }

    //Para listar los clientes
    public List<Clientes> getListaCliente() { return  listadoClientes;}


    //*******************************************************************************************************
    //Metodos de la clase Compras
    //*******************************************************************************************************

    //Para ingresar nuevas compras
    public void ingresarCompra(Compras com)
    {
        listadoCompras.add(com);
    }

    //Para listar las compras
    public List<Compras> getListadoCompras() {return listadoCompras;}

    //Suma de todas las compras
    public float totalCompras()
    {
        float total = 0;
        for (Compras c:listadoCompras)
        {
            total += c.getValor();
        }
        return total;
    }

}
