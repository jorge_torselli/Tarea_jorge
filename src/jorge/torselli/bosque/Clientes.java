package jorge.torselli.bosque;



/**
 * Created by repre on 06/07/2017.
 */
public class Clientes
{
    private String nombreCliente;
    private int codigoCliente;

    public Clientes() {
    }

    public Clientes(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public Clientes(String nombreCliente, int codigoCliente) {
        this.nombreCliente = nombreCliente;
        this.codigoCliente = codigoCliente;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public int getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(int codigoCliente) {
        this.codigoCliente = codigoCliente;
    }



}
