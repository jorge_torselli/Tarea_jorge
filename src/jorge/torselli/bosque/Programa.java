package jorge.torselli.bosque;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by repre on 06/07/2017.
 */
public class Programa
{
    public static void main(String[] args)
    {
        Empresa empresa = null;
        Scanner scan = new Scanner(System.in);
        boolean salir = false;
        int opcion;

        while (!salir)
        {
            System.out.println("1. Ingresar el nombre de la empresa");
            System.out.println("2. Ingresar nuevos clientes");
            System.out.println("3. Listar clientes");
            System.out.println("4. Ingresar compras clientes");
            System.out.println("5. Listar compras de clientes");
            System.out.println("6. Salir");

            try
            {
                System.out.println("Seleccione una de las opciones:");
                opcion = scan.nextInt();
                switch (opcion)
                {
                    case 1:
                        System.out.println("Como se llama la empresa:");
                        empresa = new Empresa(scan.next());
                        break;
                    case 2:
                        System.out.println("Ingrese el nombre del cliente:");
                        Clientes cliente = new Clientes();
                        cliente.setNombreCliente(scan.next());
                        System.out.println("Ingrese el código del cliente:");
                        cliente.setCodigoCliente(scan.nextInt());
                        empresa.insertarCliente(cliente);
                        break;
                    case 3:
                        System.out.println("Esta es la lista del los clientes:");
                        for (Clientes cl:empresa.getListaCliente())
                        {
                            System.out.printf("Nombre: " + cl.getNombreCliente() + " --  Codigo: " + cl.getCodigoCliente() + " \n");
                        }
                        System.out.println("-------------------------------------------------\n");
                        break;
                    case 4:
                        System.out.println("Ingresar las compras...\n");
                        System.out.println("Nombre del proyecto:");
                        Compras compra = new Compras();
                        compra.setProyecto(scan.next());
                        System.out.println("Ingrese el nombre del cliente");
                        compra.setCliente((scan.next()));
                        System.out.println("Valor del proyecto:");
                        compra.setValor(scan.nextFloat());
                        empresa.ingresarCompra(compra);
                        System.out.println("Se ingreso la compra.....\n");
                        break;
                    case 5:
                        System.out.println("Estas son las compras realizadas");
                        for (Compras c : empresa.getListadoCompras())
                        {
                            System.out.printf("Cliente: " + c.getCliente() + " -- Proyecto: " + c.getProyecto() + " -- Costo: Q." + c.getValor() + "\n");
                        }
                        System.out.println("El total de compras realizadas es: Q." + empresa.totalCompras());
                        System.out.println("-------------------------------------------------\n");
                        break;
                    case 6:
                        salir = true;
                        break;
                    default:
                        System.out.println("Las opciones son entre 1 y ...");
                }
            }
            catch (InputMismatchException e)
            {
                System.out.println("Debe insertar un numero");
                scan.next();
            }
        }

    }
}
