package jorge.torselli.bosque;

/**
 * Created by repre on 06/07/2017.
 */
public class Compras
{
    private String proyecto;
    private String cliente;
    private float valor;

    public Compras() {
    }

    public Compras(String proyecto, String cliente, float valor) {
        this.proyecto = proyecto;
        this.cliente = cliente;
        this.valor = valor;
    }

    public String getProyecto() {
        return proyecto;
    }

    public void setProyecto(String proyecto) {
        this.proyecto = proyecto;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public float getValor() {
        return valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }
}
